<?php

use App\Http\Controllers\SliderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
})->name('welcome');

Auth::routes();

Route::resource('user', UserController::class)->except('show');
Route::resource('slider', SliderController::class)->except('show');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
