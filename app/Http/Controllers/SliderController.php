<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequestForm;
use App\Models\Slider;
use App\Repositories\SliderRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SliderController extends Controller
{

    private $repository;

    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        return view('sliders.index', ['data' => $this->repository->getAll()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SliderRequestForm $request
     * @return Response
     */
    public function store(SliderRequestForm $request)
    {
        $this->repository->create($request->all());

        return redirect()->route('slider.index')->withStatus(__('slider successfully created.'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param Slider $slider
     * @return Application|Factory|View|Response
     */
    public function edit(Slider $slider)
    {
        return view('sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SliderRequestForm $request
     * @param $id
     * @return Response
     */
    public function update(SliderRequestForm $request ,$id)    {
        $this->repository->update($id,$request->all());

        return redirect()->route('slider.index')->withStatus(__('Slider successfully Updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return redirect()->route('slider.index')->withStatus(__('Slider successfully deleted.'));
    }
}
