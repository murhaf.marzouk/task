<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;

class SliderApiController extends Controller
{

    public function index(Request $request)
    {
        if ($request->type == 'Specific'){
            $slider = Slider::where('url','!=',null)->get();
        }
        else $slider = Slider::where('url','=',null)->get();

        return response()->json($slider);
    }
}
