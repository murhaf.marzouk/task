<?php


namespace App\Repositories;


use App\Models\Slider;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class SliderRepository extends Repository
{
    protected $model;

    public function __construct(Slider $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes)
    {
        $slider = parent::create(array_merge($attributes));
        if (array_key_exists('image',$attributes)) {
            $filename    = $attributes['image']->getClientOriginalName();
             Image::make($attributes['image'])->resize(900,600)->save(storage_path('app/public/sliders/'.now().$filename));
            $slider->image ='sliders/'.now().$filename;
            $slider->save();
        }
        return $slider;

    }

    public function update($id, array $attributes)
    {
        $slider= parent::update($id, array_merge($attributes));
        if (array_key_exists('image',$attributes)) {
            $filename    = $attributes['image']->getClientOriginalName();
            Image::make($attributes['image'])->resize(900,600)->save(storage_path('app/public/sliders/'.now().$filename));
            $slider->image ='sliders/'.now().$filename;
            $slider->save();
        }
        return $slider;
    }

}
