<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserRepository extends Repository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes)
    {
        $user = parent::create(array_merge($attributes,[
            'password'=>Hash::make($attributes['password'])
        ]));
        if (array_key_exists('image',$attributes)) {
            $user->image = Storage::disk('public')->put('users/images', $attributes['image']);
            $user->save();
        }
        return $user;
    }

    public function update($id, array $attributes)
    {
        $user= parent::update($id, array_merge($attributes,[
            'password'=>Hash::make($attributes['password'])
        ]));
        if (array_key_exists('image',$attributes)) {
        $user->image = Storage::disk('public')->put('users/images', $attributes['image']);
        $user->save();
         }
        return $user;
    }


}
