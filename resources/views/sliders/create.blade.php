@extends('layouts.app', [
    'title' => __('Slider Management'),
    'parentSection' => 'laravel',
    'elementTitle' => 'slider-management'
])
@push('css')
    <link href="{{asset('argon/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('argon/css/app.min.css')}}" rel="stylesheet" type="text/css"  id="app-stylesheet" />

@endpush
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examples') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('slider.index') }}">{{ __('Slider Management') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Add Slider') }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Slider Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('slider.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form data-parsley-validate method="post" action="{{ route('slider.store') }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Slider information') }}</h6>
                            <div class="row">
                                <div class="col-8">
                            <div class="pl-lg-4">

                                <div class="form-group">
                                    <label for="title">Title<span class="text-danger"></span></label>
                                    <input type="text" name="title" data-parsley-trigger="change"  required data-parsley-minlength="[4]" value="{{ old('title') }}"
                                           placeholder="Enter title" class="form-control" id="title">
                                    @if($errors->has('title'))
                                        <div class="error" style="color:red">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="url">Url<span class="text-danger"></span></label>
                                    <input type="url" name="url" data-parsley-trigger="change" data-parsley-type="url" value="{{ old('url') }}"
                                           placeholder="Enter name" class="form-control" id="url">
                                    @if($errors->has('url'))
                                        <div class="error" style="color:red">{{ $errors->first('url') }}</div>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="card-box">
                                                <label >Photo Slider<span class="text-danger"></span></label>
                                                <input  name="image" type="file" id="image" accept="image/*" class="dropify" data-parsley-trigger="change" required/>
                                            </div>
                                            @if($errors->has('image'))
                                                <div class="error" style="color: red">{{ $errors->first('image') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <!-- Plugins js -->
    <script src="{{asset('argon/libs/dropify/dropify.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{asset('argon/js/pages/form-fileuploads.init.js')}}"></script>


    <script src="{{asset('argon/libs/parsleyjs/parsley.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{asset('argon/js/pages/form-validation.init.js')}}"></script>

@endpush
