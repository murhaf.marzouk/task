@extends('layouts.app', [
    'title' => __('User Management'),
    'parentSection' => 'laravel',
    'elementName' => 'user-management'
])
@push('css')
    <link href="{{asset('argon/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('argon/css/app.min.css')}}" rel="stylesheet" type="text/css"  id="app-stylesheet" />
@endpush
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examples') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ __('User Management') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Edit User') }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('User Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form data-parsley-validate method="post" action="{{ route('user.update', $user) }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>
                            <div class="row">
                                <div class="col-8">
                                    <div class="pl-lg-4">

                                        <div class="form-group">
                                            <label for="name">Name<span class="text-danger"></span></label>
                                            <input type="text" name="name" data-parsley-trigger="change"  required data-parsley-minlength="[4]"value="{{ old('name',$user->name) }}"
                                                   placeholder="Enter name" class="form-control" id="name">
                                            @if($errors->has('name'))
                                                <div class="error" style="color:red">{{ $errors->first('name') }}</div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="input-email">Email<span class="text-danger"></span></label>
                                            <input type="email" name="email" data-parsley-trigger="change"  required value="{{ old('email',$user->email) }}"
                                                   placeholder="Enter name" class="form-control" id="name">
                                            @if($errors->has('email'))
                                                <div class="error" style="color:red">{{ $errors->first('email') }}</div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password<span class="text-danger"></span></label>
                                            <input id="password" type="password"  placeholder="Password" data-parsley-trigger="change" data-parsley-minlength="[6]"  value="" name="password"
                                                   class="form-control">
                                            @if($errors->has('password'))
                                                <div class="error" style="color: red">{{ $errors->first('password') }}</div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="password2">Confirm Password <span class="text-danger"></span></label>
                                            <input data-parsley-equalto="#password"  type="password" data-parsley-trigger="change" data-parsley-minlength="[6]"  value="" name="password_confirmation"
                                                   placeholder="Password" class="form-control" id="password2">
                                            @if($errors->has('password'))
                                                <div class="error" style="color: red">{{ $errors->first('password') }}</div>
                                            @endif
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="card-box">
                                                <label >Profile Photo<span class="text-danger"></span></label>
                                                <input  name="image" type="file" id="image" accept="image/*" class="dropify" data-default-file="{{$user->image()}}"/>
                                            </div>
                                            @if($errors->has('image'))
                                                <div class="error" style="color: red">{{ $errors->first('image') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <!-- Plugins js -->
    <script src="{{asset('argon/libs/dropify/dropify.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{asset('argon/js/pages/form-fileuploads.init.js')}}"></script>


    <script src="{{asset('argon/libs/parsleyjs/parsley.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{asset('argon/js/pages/form-validation.init.js')}}"></script>

@endpush
