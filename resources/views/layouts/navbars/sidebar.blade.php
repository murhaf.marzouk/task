<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner scroll-scrollx_visible">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item {{ $parentSection ?? '' == 'users' ? 'active' : '' }}">
                        <a class="nav-link collapsed" href="#navbar-users" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection ?? '' == 'users' ? 'true' : '' }}" aria-controls="navbar-users">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text">{{ __('Users') }}</span>
                        </a>
                        <div class="collapse {{ $parentSection ?? '' == 'users' ? 'show' : '' }}" id="navbar-users">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{ $elementName ?? '' == 'User' ? 'active' : '' }}">
                                    <a href="{{ route('user.index','User') }}" class="nav-link">{{ __('Users Management ') }}</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item {{ $parentSection ?? '' == 'sliders' ? 'active' : '' }}">
                        <a class="nav-link collapsed" href="#navbar-sliders" data-toggle="collapse" role="button" aria-expanded="{{ $parentSection ?? '' == 'sliders' ? 'true' : '' }}" aria-controls="navbar-sliders">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text">{{ __('Slider') }}</span>
                        </a>
                        <div class="collapse {{ $parentSection ?? '' == 'sliders' ? 'show' : '' }}" id="navbar-sliders">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{ $elementName ?? '' == 'Slider' ? 'active' : '' }}">
                                    <a href="{{ route('slider.index','Slider') }}" class="nav-link">{{ __('Slider Management ') }}</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</nav>
